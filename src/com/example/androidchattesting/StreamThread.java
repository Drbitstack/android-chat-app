package com.example.androidchattesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;

import com.example.androidchattesting.Net.ChatClientListener;
import com.example.androidchattesting.Net.Packets.DataPacket;
import com.example.androidchattesting.Net.Packets.Packet;
import com.example.androidchattesting.Utilities.SharedList;

public class StreamThread extends Thread implements ChatClientListener {

	private static final int MIN_BUFFER_CHUNK = 8192;
	private static final long MIN_PLAY_CHUNK = 1024*40;
	private Handler handler;
	private Context context;
	private Semaphore stControlSem;
	private boolean offRequest = false;
	private boolean isOn;
	
	private boolean isStreaming;


	private MediaPlayer player;

	private ArrayList<Packet> inboxList = new ArrayList<Packet>();
	private SharedList<Packet> inbox = new SharedList<Packet>(1000, inboxList);
	
	private SessionManager sessionManager = new SessionManager(this);
	
	private File downloadingMediaFile1 = new File(context.getCacheDir(),"streamingMedia_1.dat"); 
	private File downloadingMediaFile2 = new File(context.getCacheDir(),"streamingMedia_2.dat");

	
	private File activeFile = downloadingMediaFile1;
	private File inactiveFile = downloadingMediaFile1;
	
	
	private FileMonitor activeMonitor = new FileMonitor(activeFile);

	private FileMonitor inactiveMonitor = new FileMonitor(inactiveFile);
	
	//file max size large enough for 10s content
	//REFERENCE, for a 1.8GB 2hr movie, 10s = 2,684,355 bytes
	
	public StreamThread(Context context, Handler handler, Semaphore stControlSem) {
		
		this.context = context;
		this.handler = handler;
		this.stControlSem = stControlSem;
		
		initMediaPlayer();
		
	}
	

	private void swapFiles() {
		
		File temp;
		temp = activeFile;
		activeFile = inactiveFile;
		inactiveFile = temp;
		
	}
	
	private void initMediaPlayer() {
		
		player = new MediaPlayer();
		
		player.setAudioStreamType(AudioManager.STREAM_MUSIC);

		player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {	
				
				if( sessionManager.numBytesBuffered() > 0 )
				{
					bufferPlayerData();//flush the buffered data
				}
				
				if( inactiveMonitor.hasData() )
				{
					beginPlayback();
				}
			}
		});
	}

	private void beginPlayback() {
		
		swapFiles();

		//clean the file that was just completed
		resetFile( inactiveMonitor );
		
		try {
			FileInputStream fis;
			player.stop();
			fis = new FileInputStream(activeFile);
			player.setDataSource(fis.getFD());
			player.prepare();
			player.start();
		} catch (IllegalArgumentException | IllegalStateException
				| IOException e) {
			e.printStackTrace();
		}
		
	}


	private void resetFile(FileMonitor mon) {

		mon.reset();
		
	}


	public void run()
	{
		while(true)
		{
			
			checkStream();
			
			try {
				Packet packet = inbox.remove();
				
				if( offRequest )
				{
					//requesting task already took it, so it will sleep indefinitely
					try {
						setIsOn(false);
						offRequest = false;
						stControlSem.acquire();
						setIsOn(true);
						stControlSem.release();//when woken up (and the sem is acquired), just release it again
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				processPacket(packet);
				
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}

	}



	private void processPacket( Packet packet ) {
		
		if( packet instanceof DataPacket  )
		{
			processData( (DataPacket)packet );
		}
		
	}


	private void processData(DataPacket packet) {
		
		
		sessionManager.process( packet ); //manages the single streaming session, loads the data into a buffer
		
		
		
		
	}


	private void checkStream() {
		

		if ( sessionManager.numBytesBuffered() > MIN_BUFFER_CHUNK )
		{
			bufferPlayerData();
		}
			
		if( !player.isPlaying() )
		{
			if ( inactiveMonitor.getSize() > MIN_PLAY_CHUNK )
			{
				beginPlayback();
			}
		}
	
	}


	private void bufferPlayerData() {
	
		 sessionManager.bufferFileData( inactiveMonitor );   
	}


	@Override
	public void OnMessageReceive(Packet receivedPacket) {
		if( isOn() )
		{
			//process the message, for this thread, we only process data packets
			try {
				inbox.insert(receivedPacket);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void OnMessageSend(Packet sentPacket) {
		if( isOn() )
		{
			//process the message
		}
	}


	@Override
	public void OnStatusChange(int newStatus) {
		if( isOn() )
		{
			//process the client status change
		}
	}
	


	private boolean isOn() {
		return isOn;
	}

	private void setIsOn(boolean b) {
		this.isOn = b;		
	}


	public boolean turnOff(){
		
		boolean attempt = false;
		//never stall the asking task
		attempt = stControlSem.tryAcquire();
		if( attempt ) offRequest  = true;
		return attempt;
	}


	public void turnOn() {
		stControlSem.release();
	}

	
	public boolean isStreaming() {
		return isStreaming;
	}


	public void setStreaming(boolean isStreaming) {
		this.isStreaming = isStreaming;
	}

	public FileMonitor getActiveMonitor() {
		return activeMonitor;
	}


	public FileMonitor getInactiveMonitor() {
		return inactiveMonitor;
	}


	public void resetStream() {
		// TODO Auto-generated method stub
		
	}
}
