package com.example.androidchattesting;

import java.util.concurrent.Semaphore;

import com.example.androidchattesting.Net.ChatClient;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ToggleButton;

public class ListenTab extends Fragment {

	
	private static final int CHOOSE_FILE_RESULT_CODE = 20;
	private String uploadFilePath;
	private StreamThread st;
	
	private View rootView;
	private MainActivity mCallback;
	private ChatClient client;
	
	private Button uploadButton;
	private Button playButton;
	private ToggleButton  streamEnableButton;
	private Button muteButton;
	private Semaphore stControlSem = new Semaphore(1, true);
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        rootView = inflater.inflate(R.layout.main_activity_listen_tab, container, false);
        
        initButtons();
        
        initStreaming();
        
        
        return rootView;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (MainActivity) activity;
    }
    
	private void initStreaming() {
		
		st = new StreamThread( mCallback.getContext(), mCallback.getHandler(), stControlSem  );
		this.client = LoginActivity.getClient();
		client.addListener(st);
		st.start();
		
	}

	protected void streamToggleAction() {
		
		if( streamEnableButton.isChecked() )
		{
			st.turnOn();
		}
		else
		{
			st.turnOff();
		}
	}

	protected void muteButtonAction() {
		
		st.toggleMute();
		
	}

	private void playButtonAction() {
		
		st.togglePlay();
		
	}

	private void uploadAction() {
		
		playListBrowseAction();
		
		
		//TODO background upload to the server
		
	}
	

	private void playListBrowseAction() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/*");
        startActivityForResult(intent, CHOOSE_FILE_RESULT_CODE);
		
	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
    	if(resultCode != Activity.RESULT_CANCELED)
    	{

        	if(requestCode == CHOOSE_FILE_RESULT_CODE)
        	{
        		//just use it to set the text field for now
        		Uri playList = data.getData();
        		uploadFilePath = playList.getPath();
        		
        		//new UploadInBackground(handler).execute(uploadFilePath);
        	}
    	}
    }
    
    
	private void initButtons() {

		 uploadButton 	  = (Button) rootView.findViewById(R.id.listen_tab_button_upload);
		 playButton 		  = (Button) rootView.findViewById(R.id.listen_tab_button_play);
		 streamEnableButton = (ToggleButton ) rootView.findViewById(R.id.listen_tab_toggle_streaming);
		 muteButton         = (Button) rootView.findViewById(R.id.listen_tab_button_mute);
		
		uploadButton.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				uploadAction();	
			}	
		});
		playButton.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				playButtonAction();
			}
		});
		streamEnableButton.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				streamToggleAction();
			}
		});
		muteButton.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				muteButtonAction();
			}
		});
	}
	
}
