package com.example.androidchattesting;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidchattesting.Constants.StaticConstants;
import com.example.androidchattesting.Net.ChatClient;
import com.example.androidchattesting.Net.ChatClientListener;
import com.example.androidchattesting.Net.Packets.ChatPacket;
import com.example.androidchattesting.Net.Packets.Packet;

public class ChatTab extends Fragment implements ChatClientListener {

	private ChatClient client;
	private Handler handler;
	
	private Bundle loginData;
	
	private MainActivity mCallback;
	
	EditText tf;
	View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
    	handler = new Handler();
        rootView = inflater.inflate(R.layout.main_activity_chat_tab, container, false);
		
		initClient();

        initButtons();
        
        return rootView;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (MainActivity) activity;
    }
    
	@SuppressLint("NewApi")
	private void initClient() {
		
		client = LoginActivity.getClient();//don't drop the connection
		client.addListener(this); //now the client reports to the main activity window
		loginData = mCallback.getLoginData();
		client.setLoginData(loginData);//give it to the client for reconnecting on a disconnection		
		getActivity().getActionBar().setIcon(StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE);
	}
    
	private void initButtons() {


		Button sendButton = (Button) rootView.findViewById(R.id.button_chatSend);

		tf = (EditText) rootView.findViewById(R.id.textField_chatMessage);
		
		sendButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				
				String message = new String(tf.getText().toString());
				
				ChatPacket chatPacket = new ChatPacket(message, loginData.getString(LoginActivity.USERNAME_DATA));
				
				client.addOutgoingPacket(chatPacket);
				
				tf.setText("");
				//Cc.addOutgoingMessage(message.getBytes());
			}
			
		});
	}
	
	@Override
	public void OnMessageReceive(final Packet receivedPacket) {
		
		if( receivedPacket instanceof ChatPacket )
		{
			handler.post( new Runnable()
			{
				public void run()
				{
					TextView tv = (TextView) rootView.findViewById(R.id.chatBox);
					tv.setText(tv.getText().toString()+((ChatPacket) receivedPacket).getUserName() +" Says: "+ ((ChatPacket) receivedPacket).getMessage() + "\n");
				}
			});
		}
	}


	@Override
	public void OnMessageSend(final Packet sentPacket) {
		
		if( sentPacket instanceof ChatPacket )
		{
			handler.post( new Runnable()
			{
				public void run()
				{
					TextView tv = (TextView) rootView.findViewById(R.id.chatBox);
					tv.setText(tv.getText().toString()+"I Says: "+ ((ChatPacket) sentPacket).getMessage()+ "\n");
				}
			});
		}

	}


	@Override
	public void OnStatusChange(int newStatus) {
		handler.post(new Runnable()
		{
			@SuppressLint("NewApi")
			@Override
			public void run() {
				(mCallback.getActionBar()).setIcon(StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE);
			}
			
		});
		
	}
	public ChatClient getClient() {
		return client;
	}

	
}
