package com.example.androidchattesting.Net.Packets;

import java.nio.ByteBuffer;

import com.example.androidchattesting.Utilities.BufferUtils;

public class DataPacket extends Packet {

	
	private ByteBuffer dataBuf = ByteBuffer.allocate(2000);//payload buffer
	//note, actually should be 2000 - packetheader - datapacketheader
	
	private String description;
	private long count;
	private long offset;
	private long totDatasize;
	private long sessionID;
	
	private static final int NUM_STRINGS = 1;
	private static final int NUM_INTS = 0;
	private static final int NUM_LONGS = 4;
	
	public DataPacket( long count, long offset, long totDataSize ) {
		super(PacketType.DATA_PACKET.getType());
		
		this.offset = offset;
		this.count = count;
		this.totDatasize = totDataSize;
		this.setSessionID(0);
	}
	
	//PACKET RECONSTRUCTION
	public DataPacket(ByteBuffer receiveByteBuffer) 
	{
		super(PacketType.DATA_PACKET.getType());
		makeFromBuffer( receiveByteBuffer );

	}

	public void makeFromBuffer(ByteBuffer receiveByteBuffer) {
		StringBuilder sb = new StringBuilder();
		this.description = BufferUtils.unpackString(sb, receiveByteBuffer);
		this.count = receiveByteBuffer.getLong();
		this.offset = receiveByteBuffer.getLong();
		this.totDatasize = receiveByteBuffer.getLong();
		this.setSessionID(receiveByteBuffer.getLong());
		this.dataBuf.put(receiveByteBuffer);//copy the rest to my internal data buffer
	}
	

	public ByteBuffer getDataBuf() {
		return dataBuf;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public long getTotDatasize() {
		return totDatasize;
	}

	public void setTotDatasize(long totDatasize) {
		this.totDatasize = totDatasize;
	}

	@Override
	public void pack(ByteBuffer bb) {
		
		getHeader().setId(0);
		getHeader().setKey(PacketHeader.MESSAGE_KEY);
		getHeader().setSize(getPackedSize());
		getHeader().setType(getType());
		
		//and pack it
		getHeader().pack(bb);
		BufferUtils.packString(description, bb);
		bb.putLong(count);
		bb.putLong(offset);
		bb.putLong(totDatasize);
		bb.putLong(0);//outgoing sessionID is always 0 for clients;
		bb.put(dataBuf);
	}

	@Override
	public int getPackedSize() {
		
		int descriptionSize = description.length();
		
		//string lengths * length of java char + #strings*length encoded int size of 4
		int totalStringsSize = (descriptionSize)*2;
		int totalPrimitiveDigitSize = (NUM_STRINGS)*4 + 8*NUM_LONGS; //1 string encoding + 3 longs
		
		int totalMessageSize = (int) (totalStringsSize + totalPrimitiveDigitSize + this.count);
		return totalMessageSize + PacketHeader.LENGTH;
	}

	public long getSessionID() {
		return sessionID;
	}

	public void setSessionID(long sessionID) {
		this.sessionID = sessionID;
	}
	
	

}
