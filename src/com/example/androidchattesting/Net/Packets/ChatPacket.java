package com.example.androidchattesting.Net.Packets;

import java.nio.ByteBuffer;

import com.example.androidchattesting.Utilities.BufferUtils;

public class ChatPacket extends Packet{

	private static final int NUM_STRINGS = 2;
	private static final int NUM_INTS = 0;
	private static final int NUM_LONGS = 0;
	
	public String message;
	public String userName;
	
	public ChatPacket(String message, String username) {
		super(PacketType.CHAT_PACKET.getType());
		this.message = message;
		this.userName = username;
	}

	public static Packet makePacketFromBuffer(ByteBuffer receiveByteBuffer) {
		
		String messageString[] = new String[NUM_STRINGS];
		int idx = 0;
		boolean ok = true;
		
		StringBuilder sb = new StringBuilder();
		
		do{
			messageString[idx] = BufferUtils.unpackString( sb, receiveByteBuffer );
			
			if( messageString[idx] == null )
			{
				ok = false;
			}
			
			idx++;
		}while( ok && idx < NUM_STRINGS );
		
		if( ok )
		{								//message       username
			return new ChatPacket(messageString[1],messageString[0]);
		}
		else
		{
			return null;
		}
		
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void pack(ByteBuffer bb) {


		
		header.setId(0);
		header.setKey(PacketHeader.MESSAGE_KEY);
		header.setSize(getPackedSize()); 
		header.setType(getType());

		header.pack(bb);
		BufferUtils.packString( userName, bb );
		BufferUtils.packString( message, bb );

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public int getPackedSize() {
		
		int stringCount = 0;
		int totalStringsSize = 0;
		int totalIntSize = 0;
		int totalMessageSize = 0;
		
		
		totalStringsSize += message.length();
		totalStringsSize += userName.length();
		
		totalStringsSize = (totalStringsSize)*2 + (NUM_STRINGS)*4;
		totalMessageSize = totalStringsSize + totalIntSize;
		
		return totalMessageSize + PacketHeader.LENGTH;
	}


}
