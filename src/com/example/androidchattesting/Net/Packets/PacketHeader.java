package com.example.androidchattesting.Net.Packets;

import java.nio.ByteBuffer;

import com.example.androidchattesting.Net.Packets.Packet.PacketType;


public class PacketHeader {
	public static final int MESSAGE_KEY = 3984762;
	public static final int LENGTH = 4*4; //the size of the header is 12 bytes
	protected int key;
	protected int id;
	protected int type;
	protected int size; //includes the header

	public PacketHeader()
	{
		this(0, PacketType.INVALID_PACKET.getType(), 128);
	}
	
	public PacketHeader(  int id, int type, int size)
	{
		this.key = MESSAGE_KEY;
		this.id = id;
		this.type = type;
		this.size = size;
	}
	
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	
	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void pack( ByteBuffer bb )
	{
		bb.putInt(getKey());
		bb.putInt(getId());
		bb.putInt(getType());
		bb.putInt(getSize());
	}

	public void clear() {
		setSize(0);
		setId(0);
		setType(PacketType.INVALID_PACKET.getType());
		setKey(0);
		
	}

}