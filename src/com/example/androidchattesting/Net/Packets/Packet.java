package com.example.androidchattesting.Net.Packets;

import java.io.Serializable;
import java.nio.ByteBuffer;

public abstract class Packet implements Serializable {

	private static final long serialVersionUID = 4561089399746790706L;

	public static final int NUM_PACKET_TYPES = 3;
	
	PacketHeader header = new PacketHeader();
	
	public enum PacketType {
		
		REGISTER_PACKET(01),
		CHAT_PACKET(02),
		INVALID_PACKET(-01), 
		DATA_PACKET(03);
		
		private int type;
		PacketType( int type)
		{
			this.type = type;
		}
		public int getType()
		{
			return this.type;
		}

	}
	
	public byte type;

	private boolean inTxBuffer;
	
	protected PacketHeader getHeader() {
		return header;
	}
	
	public Packet( int type )
	{
		this.type = (byte) type;
		setInTxBuffer(false);
	}

	public byte getType() {
		return type;
	}
	
	public static PacketType lookupType(Packet packet) 
	{
		int packetType = (int)packet.getType();
		return lookupType(packetType);
	}
	
	public static PacketType lookupType(int packetType) {
		for( PacketType type : PacketType.values()  )
		{
            if (type.getType() == packetType) {
                return type;
            }
		}
		return PacketType.INVALID_PACKET;
	}

	public abstract void pack ( ByteBuffer bb ); //places the packet ready to send into the given buffer

	public static int extractHeader(PacketHeader header, int limit, ByteBuffer bb) {
		
		int startPosition = bb.position();//save the buffer position
		
		bb.position(0); //move to the beginning of the buffer
		int headerStart = -1;
		while( bb.position() + 4 < limit )
		{
			if( bb.getInt() == PacketHeader.MESSAGE_KEY )
			{
				headerStart = bb.position() - 4 ;
				break;
			}
		}
	
		if( headerStart >= 0 )
		{
			bb.position(headerStart);
			header.setKey(bb.getInt());
			header.setId(bb.getInt());
			header.setType(bb.getInt());
			header.setSize(bb.getInt());
		}

		bb.position(startPosition);
		
		return headerStart;
	}

	public abstract int getPackedSize();

	public void setInTxBuffer( boolean b )
	{
		this.inTxBuffer = b;
	}
	
	public boolean inTxBuffer() {
		return inTxBuffer;
	}
}
