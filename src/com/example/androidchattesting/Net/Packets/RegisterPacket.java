package com.example.androidchattesting.Net.Packets;

import java.nio.ByteBuffer;

import com.example.androidchattesting.Utilities.BufferUtils;

public class RegisterPacket extends Packet {

	private static final int NUM_STRINGS = 2;
	public String username;
	public String password;
	public int validId;

	public RegisterPacket( String username, String password ) 
	{
		this(username, password, 1);
	}
	
	public RegisterPacket(String username, String password, int validId) {
		super(PacketType.REGISTER_PACKET.getType());
		
		this.username = username;
		this.password = password;
		this.validId = validId;
	}
	
	//PACKET RECONSTRUCTION
	public static RegisterPacket makePacketFromBuffer(ByteBuffer receiveByteBuffer) 
	{
		String messageString[] = new String[NUM_STRINGS];
		int idx = 0;
		boolean ok = true;
		
		StringBuilder sb = new StringBuilder();
		
		int validId = receiveByteBuffer.getInt();
		
		do{
			messageString[idx] = BufferUtils.unpackString( sb, receiveByteBuffer );
			
			if( messageString[idx] == null )
			{
				ok = false;
			}
			
			idx++;
		}while( ok && idx < NUM_STRINGS );
		
		if( ok )
		{								//username		password
			return new RegisterPacket(messageString[0],messageString[1], validId);
		}
		else
		{
			return null;
		}
	}

	//PACKET DECONSTRUCTION
	@Override
	public void pack(ByteBuffer bb) {

		
		header.setId(0);
		header.setKey(PacketHeader.MESSAGE_KEY);
		header.setSize(getPackedSize());
		header.setType(getType());
		
		//and pack it
		header.pack(bb);
		bb.putInt(validId);
		BufferUtils.packString(username, bb);
		BufferUtils.packString(password, bb);
		
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getValidId() {
		return validId;
	}

	public void setValidId(int validId) {
		this.validId = validId;
	}

	@Override
	public int getPackedSize() {
		
		int usernameSize = username.length();
		int passwordSize = password.length();
		
		//string lengths * length of java char + #strings*length encoded int size of 4
		int totalStringsSize = (usernameSize+passwordSize)*2;
		int totalIntSize = (NUM_STRINGS)*4 + 4; //2 string encodings + validId int 
		
		int totalMessageSize = totalStringsSize + totalIntSize;		
		
		return totalMessageSize + PacketHeader.LENGTH;
	}


}
