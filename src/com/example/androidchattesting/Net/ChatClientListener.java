package com.example.androidchattesting.Net;

import com.example.androidchattesting.Net.Packets.Packet;

public interface ChatClientListener {

	public void OnMessageReceive(Packet receivedPacket);
	public void OnMessageSend(Packet sentPacket);
	
	public void OnStatusChange( int newStatus );

}
