package com.example.androidchattesting.Net;

import java.util.ArrayList;

import com.example.androidchattesting.Net.Packets.Packet;

public class ExtendedChatClientListener implements ChatClientListener {

	
	private ArrayList<ChatClientListener> listeners = new ArrayList<ChatClientListener>();
	
	@Override
	public void OnMessageReceive(Packet receivedPacket) {
		for( ChatClientListener listener : listeners )
		{
			listener.OnMessageReceive(receivedPacket);
		}
	}

	@Override
	public void OnMessageSend(Packet sentPacket) {
		for( ChatClientListener listener : listeners )
		{
			listener.OnMessageSend(sentPacket);
		}
	}

	public ArrayList<ChatClientListener> getListeners() {
		return listeners;
	}

	public void addListener(ChatClientListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(ChatClientListener listener) {
		this.listeners.remove(listener);
	}

	@Override
	public void OnStatusChange(int newStatus) {
		
		for( ChatClientListener listsner : listeners )
		{
			listsner.OnStatusChange(newStatus);
		}
	}
}
