package com.example.androidchattesting.Net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import com.example.androidchattesting.LoginActivity;
import com.example.androidchattesting.R;
import com.example.androidchattesting.Constants.StaticConstants;
import com.example.androidchattesting.Net.Packets.ChatPacket;
import com.example.androidchattesting.Net.Packets.Packet;
import com.example.androidchattesting.Net.Packets.Packet.PacketType;
import com.example.androidchattesting.Net.Packets.PacketHeader;
import com.example.androidchattesting.Net.Packets.RegisterPacket;
import com.example.androidchattesting.Utilities.IOTracker;

public class ChatClient extends Thread{

	
	//the client general states
	private static final int DISCONNECTED = 6565168;
	private static final int CONNECTED = 343521;
	
	
	//substates
	private static final int CONNECTED_NOT_REGISTERED = 5621732;
	private static final int CONNECTED_REGISTERED = 2354376;
	
	private static final int READ_BUFF_SZ = 8192;
	private static final int SEND_BUF_SZ = 8192;
	private static final int REGISTER_TIMEOUT = 500; //ms


	int clientState = DISCONNECTED;
	
	//a local indication of what the server will allow me to do
	int clientSubState = CONNECTED_NOT_REGISTERED;

	private SocketChannel clientChannel;
	private SocketAddress serverAddress;
	private ExtendedChatClientListener listener;
	private int serverPort;
	
	private Selector selector;
	private int selections;
	private int timeout = 0;
	
	private PacketHeader packetHeader = new PacketHeader();
	
	private ByteBuffer receiveByteBuffer = ByteBuffer.allocate(READ_BUFF_SZ);
	private ByteBuffer sendByteBuffer    = ByteBuffer.allocate(SEND_BUF_SZ);
	
	private IOTracker rxTracker = new IOTracker();
	private IOTracker txTracker = new IOTracker();
	
	private ArrayList<Packet> sendQueue = new ArrayList<Packet>(); //packet FIFO
	
	private Bundle loginData;
	
	private long sendRegisterTime;
	private boolean loginNotYetAttempted = true;
	private boolean hasARegisterListener = false;
	private int prevClientState = 0;
	
	private Handler parentHandler;
	private ActionBar actionBar;


	public ChatClient( ChatClientListener listener, String string, int i) {
		this.serverPort = i;
		this.listener = new ExtendedChatClientListener();
		this.serverAddress = new InetSocketAddress(string, i);
	}


	@SuppressLint("ShowToast")
	public void run()
	{
		
		try {
			selector = Selector.open();
		} catch (IOException e) {
			e.printStackTrace();
		}

		while(true)
		{
			
			if( prevClientState != clientState )
			{
				doClientStateChange( prevClientState, clientState );
				prevClientState = clientState;
			}

			switch( clientState )
			{
				case DISCONNECTED:
					doDisconnected();
					break;
				case CONNECTED:
					doConnected();
					break;
				default:
					break;
			}
			
			
		}
		
	}
	
	
	@SuppressLint("NewApi")
	private void doClientStateChange( int oldState, int newState ) {
		
		if( oldState == CONNECTED && newState == DISCONNECTED )
		{
			StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE = R.drawable.main_menu_status_icon_red;
			
		}
		else if( oldState == DISCONNECTED && newState == CONNECTED )
		{
			StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE = R.drawable.main_menu_status_icon_green;
		}
		
		listener.OnStatusChange(newState);
		
	}


	private void doConnectedNotRegistered() {
		
			String username = loginData.getString(LoginActivity.USERNAME_DATA);
			String password = loginData.getString(LoginActivity.PASSWORD_DATA);
			
			Packet RegisterPacket = new RegisterPacket(username, password);
			
			if( !hasARegisterListener )
			{
				listener.addListener(new ChatClientListener()
				{

					@Override
					public void OnMessageReceive(Packet receivedPacket) {
						if( receivedPacket instanceof RegisterPacket )
						{
							if(((RegisterPacket)receivedPacket).getValidId() == 1 ) //TODO some kind of actual, meaningful validation...
							{
								setClientSubState(CONNECTED_REGISTERED);
							}
							listener.removeListener(this);//discard itself after it's received a reply
							hasARegisterListener = false;
							loginNotYetAttempted = false;//login has now been attempted
						}
					}

					@Override
					public void OnMessageSend(Packet sentPacket) {
						return;
					}

					@Override
					public void OnStatusChange(int newStatus) {
						return;
					}
					
				});
				
				hasARegisterListener = true;
			}
			
			addOutgoingPacket(RegisterPacket);
			sendRegisterTime = System.currentTimeMillis();
		
	}


	private boolean connect() {
		
		boolean success = false;
		
		try {
			
			if( clientChannel == null )
			{
				clientChannel = selector.provider().openSocketChannel();
			}
			
			if( !clientChannel.isOpen() )
			{
				clientChannel = SocketChannel.open();
			}
			
			if( clientChannel.isBlocking() )
			{
				clientChannel.configureBlocking(false);
			}
			
			if( !clientChannel.isConnected() )
			{
				if( clientChannel.isConnectionPending() )
				{
					clientChannel.finishConnect();
				}
				else
				{
					clientChannel.connect(serverAddress);
				}
			}
			
			if( clientChannel.isConnected() )
			{	
				if(	!clientChannel.isRegistered() )
				{
					clientChannel.register(selector, SelectionKey.OP_READ);
				}
				success = true;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return success;
		
	}


	private void doDisconnected() {
		try {
			
			connect();
			
			if ( clientChannel.isConnected() )
			{
				setClientState(CONNECTED);
			}
			
			Thread.sleep(150);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	private void doConnected() {
		
		boolean doTx = false;
		
		try {
			
			if( getClientSubState() == CONNECTED_NOT_REGISTERED 
				&& loginNotYetAttempted
				&& loginData != null  //having login data signifies that this client was once validated already by the server
				&& (System.currentTimeMillis() - sendRegisterTime) >= REGISTER_TIMEOUT) //need to register
			{
				doConnectedNotRegistered();//send a register packet
			}
			
			//if there are messages in the outbox, perform a tx operation
			if( sendQueue.size() > 0 )
			{
				doTx = true;
				selections = selector.selectNow(); 
			}
			else
			{
				selections = selector.select(); 
			}
			
			if(selections > 0)
			{
				Set<SelectionKey> keys = selector.selectedKeys();
				synchronized (keys)
				{
					//TODO for loop not really necessary, since we are only talking to the server..
					for (Iterator<SelectionKey> iter = keys.iterator(); iter.hasNext();) 
					{
						SelectionKey selectionKey = iter.next();
						iter.remove();
						int ops = selectionKey.readyOps();
						if ((ops & SelectionKey.OP_READ) == SelectionKey.OP_READ) 
						{
							SocketChannel receiveChannel = (SocketChannel)selectionKey.channel();

							handleRead(receiveChannel);

						}
					}
				}
			}
			
			if( doTx )
			{
				sendQueuedPackets();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void handleRead(SocketChannel receiveChannel) {
		
		//hand it off to the listener
		int bytesRead = 0;
		int totalBytes = 0;
		
		try {

			// do a read
			bytesRead = receiveChannel.read(receiveByteBuffer);
			totalBytes = bytesRead + rxTracker.getPos();
			
			if( bytesRead > 0 )
			{
			
				msgExtract( totalBytes );
			}
			else
			{
				close( clientChannel );
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	private void msgExtract(int totalBytes) {
		
		if( rxTracker.isIOInProgress() ) //last receive didn't complete the message
		{
			if( totalBytes >= packetHeader.getSize() ) //if rx is in progress, our header is current
			{
				completeRx(totalBytes);
				
				//if after a packet was removed, we still have bytes in the buffer, recurse
				if( rxTracker.getPos() > PacketHeader.LENGTH )
				{
					msgExtract( rxTracker.getPos() );
				}
				return;
			}
			else
			{
				rxTracker.setPos(totalBytes);
			}
		}
		else
		{	
			if( totalBytes > PacketHeader.LENGTH )
			{
				Packet.extractHeader(packetHeader, totalBytes, receiveByteBuffer);
				if( validatePacket(packetHeader) )
				{
					if( totalBytes >= packetHeader.getSize() )
					{
						completeRx(totalBytes);
						//if after a packet was removed, we still have bytes in the buffer, recurse
						if( rxTracker.getPos() > PacketHeader.LENGTH )
						{
							msgExtract( rxTracker.getPos() );
						}
						return;
					}
					else
					{
						rxTracker.setIOInProgress(true);
					}
				}
				else
				{
					// no header found in all of the data, and we weren't in the middle of rx. 
					//drop the data
					clearRx();
					return;
				}
			}

			rxTracker.setPos(totalBytes);

		}
	}


	private void clearRx() {
		rxTracker.reset();
		packetHeader.clear();
		rxTracker.setIOInProgress(false);
		receiveByteBuffer.position(0).limit(receiveByteBuffer.capacity());
	}
	
	private void clearTx() {
		txTracker.reset();
		txTracker.setIOInProgress(false);
		sendByteBuffer.position(0).limit(sendByteBuffer.capacity());
	}


	private void completeRx( int totalBytes ) {
		//we have the full packet here
		Packet packet = null;
		receiveByteBuffer.position(packetHeader.getSize());
		receiveByteBuffer.flip();
		receiveByteBuffer.position(PacketHeader.LENGTH);//skip the header

		switch( Packet.lookupType(packetHeader.getType()) )
		{
			case REGISTER_PACKET:
				packet = RegisterPacket.makePacketFromBuffer(receiveByteBuffer);
				break;
			case CHAT_PACKET:
				packet = ChatPacket.makePacketFromBuffer(receiveByteBuffer);
				break;
			default:
				break;
		}
		
		rxTracker.setPos( totalBytes - packetHeader.getSize() );
		rxTracker.setIOInProgress(false);
		receiveByteBuffer.compact();//remove the packet from the rx buffer
		if( packet != null ) listener.OnMessageReceive(packet);
	}


	private boolean validatePacket(PacketHeader packetHeader) {
		
		if( packetHeader.getKey() != PacketHeader.MESSAGE_KEY ) return false; 
		
		if( packetHeader.getSize() < PacketHeader.LENGTH ) //all packets are at least header long
		{
			return false;
		}
		
		if( Packet.lookupType(packetHeader.getType()) == PacketType.INVALID_PACKET ) 
		{
			return false;
		}
		return true;
	}


	private void sendQueuedPackets() {
		
		int bytesWritten = 0;
		int packetSize = 0;
		int totalSend = 0;
		int bytesPacked = 0;
		int packetInProgressSize = 0;
		boolean dataWasTxedThatMightIndicateACompleteSend = false;
		
		//guaranteed to have a packet in queue when entering this function
		Packet packetInProgress = sendQueue.get(0);
		packetInProgressSize = packetInProgress.getPackedSize();

		int queueSize = sendQueue.size();
		
		for( int i=0;i<queueSize;++i  )
		{
			Packet bufPacket = sendQueue.get(i);
			
			if( !bufPacket.inTxBuffer() )
			{
				packetSize = bufPacket.getPackedSize();
				
				if( txTracker.getPos() + packetSize < SEND_BUF_SZ ) //buffer whole packets only
				{
					bufPacket.pack(sendByteBuffer);
					bytesPacked += packetSize;
					txTracker.setPos(txTracker.getPos() + packetSize);
					txTracker.packOne();
					bufPacket.setInTxBuffer(true);
					txTracker.setIOInProgress(true);
				}
				else
				{
					break;
				}
			}

		}
		
		sendByteBuffer.limit(txTracker.getPos());
		sendByteBuffer.position(0);
		
		try {
			if ((bytesWritten = clientChannel.write(sendByteBuffer)) == 0);
		} catch (IOException e) {
			//e.printStackTrace();
			close( clientChannel );
		}
		
		sendByteBuffer.compact();
		txTracker.setPos(txTracker.getPos() - bytesWritten);
		
		totalSend = txTracker.getSize() + bytesWritten;
		
		txTracker.setSize(totalSend);

		do
		{
					
			if( txTracker.getSize() >= packetInProgress.getPackedSize() )
			{
				listener.OnMessageSend(packetInProgress);
				sendQueue.remove(0);
				txTracker.unpackOne();
				txTracker.setSize( txTracker.getSize() - packetInProgressSize );
				
				if( txTracker.getSize() > 0 && txTracker.getNumPacked() > 0 ) //are there more packets in the buffer?
				{
					dataWasTxedThatMightIndicateACompleteSend = true; //then come back and check for another packet
					packetInProgress = sendQueue.get(0);
				}
			}
			else
			{
				dataWasTxedThatMightIndicateACompleteSend = false; //don't come back
			}
			
		}while( dataWasTxedThatMightIndicateACompleteSend );

	}


	private void close(SocketChannel channel) {
		
		try {
			channel.close();
			clearRx(); //TODO maybe save state for when reconnecting..
			clearTx();
		} catch (IOException e) {
			e.printStackTrace();
		}
		setClientState(DISCONNECTED);
	}


	public void addOutgoingPacket( Packet packet )
	{
		sendQueue.add(packet);
		selector.wakeup();
	}
	
	
	
	public ChatClientListener getListener() {
		return listener;
	}


	public void addListener(ChatClientListener listener) {
		this.listener.addListener(listener);
	}
	
	public void removeListener(ChatClientListener listener) {
		this.listener.removeListener(listener);
	}
	
	public int getClientState() {
		return clientState;
	}

	public void setClientState(int clientState) {
		this.clientState = clientState;
		if( clientState == DISCONNECTED )
		{
			setClientSubState(CONNECTED_NOT_REGISTERED);//every disconnection TODO this is bad practice
		}
	}
	
	public int getClientSubState() {
		return clientSubState;
	}

	public void setClientSubState(int clientSubState) {
		this.clientSubState = clientSubState;
		
		if( clientSubState == CONNECTED_NOT_REGISTERED )
		{
			this.loginNotYetAttempted = true; //reset the one shot attempt
		}
	}

	public void setLoginData(Bundle loginData) {
		this.loginData = loginData;
	}


	public Handler getParentHandler() {
		return parentHandler;
	}


	public void setParentHandler(Handler parentHandler) {
		this.parentHandler = parentHandler;
	}


	public ActionBar getActionBar() {
		return actionBar;
	}


	public void setActionBar(ActionBar actionBar) {
		this.actionBar = actionBar;
	}
	


}
