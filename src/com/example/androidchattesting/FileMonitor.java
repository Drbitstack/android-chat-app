package com.example.androidchattesting;

import java.io.File;

public class FileMonitor {
	
	private File file;
	private boolean hasData;
	private long size;
	private long position;
	
	public FileMonitor(File file) {
		this.file = file;
	}
	public boolean hasData() {
		return hasData;
	}
	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
		if(size == 0)
		{
			hasData = false;
		}
	}
	public void addSize( long add )
	{
		this.size += add;
		if(!hasData )
		{
			hasData = true;
		}
	}

	public long getPosition() {
		return position;
	}
	public void setPosition(long position) {
		this.position = position;
	}
	public void reset() {
		setHasData(false);
		setPosition(0);
		setSize(0);
		
	}
	public File getFile() {
		return file;
	}
	

}
