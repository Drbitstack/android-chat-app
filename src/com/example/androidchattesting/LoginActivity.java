package com.example.androidchattesting;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.androidchattesting.R;
import com.example.androidchattesting.Async.BackgroundPacket;
import com.example.androidchattesting.Async.BgPacketListener;
import com.example.androidchattesting.Constants.StaticConstants;
import com.example.androidchattesting.Net.ChatClient;
import com.example.androidchattesting.Net.ChatClientListener;
import com.example.androidchattesting.Net.Packets.Packet;
import com.example.androidchattesting.Net.Packets.RegisterPacket;

public class LoginActivity extends Activity implements BgPacketListener, ChatClientListener {
	
	
	public static final String LOGIN_DATA 	 = "com.millstudios.androidchattesting.loginData";
	public static final String USERNAME_DATA = "com.millstudios.androidchattesting.usernameData";
	public static final String PASSWORD_DATA = "com.millstudios.androidchattesting.passwordData";
	
	private static ChatClient client;
	//let everyone use it
	public static ChatClient getClient() {
		return client;
	}

	private boolean registerInfoAvailable;
	private Handler handler;
	private Activity thisActivity;
	private String username, password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		
		StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE = R.drawable.main_menu_status_icon_red;
		
		handler = new Handler();
		
		thisActivity = this;
		
		initClient();
		
		initButtons();

		
	}
	
	private void initButtons() {
		
		Button login = (Button) findViewById(R.id.button_login);
		
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				loginAction();
				
			}
		});
		
	}

	private void initClient() {

		client = new ChatClient( null, "192.168.1.110", 6567 );//"98.27.204.85"
		client.start();
		client.addListener(this);
		
	}

	private void loginAction() {
		
		EditText etU = (EditText) findViewById(R.id.editText_login_username);
		EditText etP = (EditText) findViewById(R.id.editText_login_password);

		username = etU.getText().toString();
		password = etP.getText().toString();

		tryLogin( username, password );
		
	}

	private void tryLogin( String username, String password ) {
		
		RegisterPacket registerPacket = new RegisterPacket(username, password);
		
		new BackgroundPacket(this, client, 10000).execute( (Packet)registerPacket );
		
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onResume()
	{
		super.onResume();
		//refresh the icon
		getActionBar().setIcon(StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE);
	}

	
	@Override
	public void notifyBpDone( boolean success ) {
		
		if( success )
		{
			
			Intent intent = new Intent(thisActivity, MainActivity.class);
			Bundle bundle = new Bundle();
			
			bundle.putString(USERNAME_DATA, username);
			bundle.putString(PASSWORD_DATA, password);
			
			intent.putExtra(LOGIN_DATA, bundle);
			
			startActivity(intent);
		}
		else
		{
			Toast.makeText(thisActivity, "Login failed: ", Toast.LENGTH_LONG);
		}
		
	}

	@Override
	public void OnMessageReceive(Packet receivedPacket) {
		return;		
	}

	@Override
	public void OnMessageSend(Packet sentPacket) {
		return;
	}

	@Override
	public void OnStatusChange(int newStatus) {
		handler.post(new Runnable()
		{
			@SuppressLint("NewApi")
			@Override
			public void run() {
				getActionBar().setIcon(StaticConstants.CONNECTION_STATUS_ICON_DRAWABLE);
			}
			
		});
	}

}
