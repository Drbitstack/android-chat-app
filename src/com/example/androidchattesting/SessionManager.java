package com.example.androidchattesting;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import com.example.androidchattesting.Net.Packets.DataPacket;
import com.example.androidchattesting.Utilities.IOTracker;

public class SessionManager {

	private StreamThread streamThread;
	private ByteBuffer streamBuff = ByteBuffer.allocateDirect(2*1024*1024);

	private int numBytesBuffered;
	
	private IOTracker rxTracker = new IOTracker();

	boolean sessionIsActive;
	
	private FileMonitor activeMonitor, inactiveMonitor;
	
	private long sessionID;

	
	
	public SessionManager(StreamThread streamThread) {
		this.streamThread = streamThread;
		this.activeMonitor = streamThread.getActiveMonitor();
		this.inactiveMonitor = streamThread.getInactiveMonitor();
		rxTracker.reset();
	}

	public void process(DataPacket packet) {
		boolean resetStream = false;
		
		if( packet.getSessionID() == sessionID )
		{
			//continuation, keep going
			

		}
		else  //close the current stream and start a new one
		{
			sessionID = packet.getSessionID();
			
			rxTracker.reset();
			
			resetStream = true;
			
		}
		
		packetDataToBuffer(packet);
		
		if(resetStream)streamThread.resetStream();
		
		
	}

	private void packetDataToBuffer(DataPacket packet) {
		
		int prevPos = rxTracker.getSize();
		
		streamBuff.position(prevPos); //set the cursor
		
		streamBuff.put(packet.getDataBuf());
		
		rxTracker.addSize(streamBuff.position() - prevPos);
		
	}

	public long bufferFileData(FileMonitor outMon) {
		
		long bytes_flushed = rxTracker.getSize();
		
		if( rxTracker.getSize() > 0 )
		{
			try {
				

				RandomAccessFile rFile = new RandomAccessFile(outMon.getFile(), "rw");
				
				rFile.seek( outMon.getSize() );
				streamBuff.flip();
				rFile.write(streamBuff.array());
				outMon.addSize(rxTracker.getSize());
				
				rxTracker.reset();
				streamBuff.position(0).limit(streamBuff.capacity());
					
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return bytes_flushed;
		
	}

	public int numBytesBuffered() {
		return rxTracker.getSize();
	}

	
	
	
	

}
