package com.example.androidchattesting.Async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.androidchattesting.Net.ChatClient;
import com.example.androidchattesting.Net.ChatClientListener;
import com.example.androidchattesting.Net.Packets.Packet;
import com.example.androidchattesting.Net.Packets.RegisterPacket;

public class BackgroundPacket extends AsyncTask<Packet, Integer, Boolean> implements ChatClientListener {
	
	private ChatClient client;
	private Context context;
	private int timeout; //ms
	private boolean receivedReply;
	private boolean replyResult = false;
	
	private ProgressDialog dialog;

	public BackgroundPacket(Context context, ChatClient client, int timeout )
	{
		this.context = context;
		this.client = client;
		this.timeout = timeout;
		
		dialog = new ProgressDialog(context);
	}
	
    /** progress dialog to show user that the backup is processing. */
    /** application context. */
    @Override
    protected void onPreExecute() {
        this.dialog.setMessage("Logging in...");
        this.dialog.show();
        client.addListener(this);
    }
	
	@Override
	protected Boolean doInBackground(Packet... params) {

		long timeout_time = System.currentTimeMillis();
		
		timeout_time = timeout_time + timeout;
		
		client.addOutgoingPacket( (Packet)params[0] );
		
		//and spin here for response or timeout...
		while( (System.currentTimeMillis() < timeout_time) && !receivedReply )
		{
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		return replyResult;
	}

	@Override
	public void OnMessageReceive(Packet receivePacket) {
		
		if( receivePacket instanceof RegisterPacket )
		{
			receivedReply = true;
			if( ((RegisterPacket)receivePacket).getValidId() == 1 ) //since its an int boolean...
			{
				replyResult = true;
			}
		}
	}

	@Override
	public void OnMessageSend(Packet sendPacket) {
		
		
	}

    @Override
    protected void onPostExecute(final Boolean success) {

    	client.removeListener(this);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        ((BgPacketListener)(this.context)).notifyBpDone(success);

    }

	@Override
	public void OnStatusChange(int newStatus) {
	}

	

}
