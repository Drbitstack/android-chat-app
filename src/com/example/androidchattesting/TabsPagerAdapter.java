package com.example.androidchattesting;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class TabsPagerAdapter extends FragmentPagerAdapter {
 
	 ChatTab chatTab;
	 ListenTab listenTab;
	
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        
        //start all of the tabs here
       
        chatTab   = new ChatTab(); 
        listenTab = new ListenTab();
        
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            return chatTab;
        case 1:
            return listenTab;
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
 
}