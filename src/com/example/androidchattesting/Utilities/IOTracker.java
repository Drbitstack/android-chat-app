package com.example.androidchattesting.Utilities;

public class IOTracker {

	
	private int     pos;
	private boolean ioInProgress;
	private int 	size;
	
	private int numPacked;
	
	
	
	public IOTracker()
	{
		setPos(0);
		setIOInProgress(false);
		setSize(0);
		setNumPacked(0);
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int bufferedBytes) {
		this.pos = bufferedBytes;
	}

	public boolean isIOInProgress() {
		return ioInProgress;
	}

	public void setIOInProgress(boolean ioInProgress) {
		this.ioInProgress = ioInProgress;
	}

	public void reset() {
		setPos(0);
		setIOInProgress(false);
		setSize(0);
		setNumPacked(0);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getNumPacked() {
		return numPacked;
	}

	public void setNumPacked(int numPacked) {
		this.numPacked = numPacked;
	}

	public void packOne() {
		this.numPacked++;
	}

	public void unpackOne() {
		this.numPacked--;
	}

	public void addPos(int numBytesBuffered) {
		this.pos+=numBytesBuffered;
	}

	public void addSize(int i) {
		this.size+=i;
	}
	
}
