package com.example.androidchattesting.Utilities;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;


public class SharedList<T>{
	
	private Semaphore listAccessor;
	private Semaphore slots;
	private Semaphore items;
	
	private ArrayList<T> itemList;
	
	private int capacity;
	
	public SharedList( int capacity, ArrayList<T> theList)
	{
		this.listAccessor = new Semaphore(1, true);
		this.slots = new Semaphore(capacity, true);
		this.items = new Semaphore(0, true);
		makeList(theList);
	}

	public Semaphore getListAccessor() {
		return listAccessor;
	}

	public Semaphore getSlots() {
		return slots;
	}

	public Semaphore getItems() {
		return items;
	}

	public void makeList( ArrayList<T> list )
	{
		this.itemList = list;
	} 
	
	public void insert( T o ) throws InterruptedException
	{
		slots.acquire();	//take a slot
		listAccessor.acquire();
		itemList.add(o);
		listAccessor.release();
		items.release();	//free an item
	}
	
	public boolean tryInsert( T o ) throws InterruptedException
	{
		boolean attempt = listAccessor.tryAcquire();
		
		if(attempt)
		{
			itemList.add(o);
			listAccessor.release();
		}
		
		return attempt;
	}
	

	public T remove() throws InterruptedException
	{
		T o = null;
		items.acquire();    //take an item
		listAccessor.acquire();
		o = itemList.get(0);
		itemList.remove(o);
		listAccessor.release();
		slots.release();	//free a slot
		return o;
	}
	
	public T tryRemove() throws InterruptedException
	{
		boolean attempt = items.tryAcquire();
		T o = null;
		
		if( attempt )
		{
			listAccessor.acquire();
			o = itemList.remove(0);
			listAccessor.release();
			slots.release();
		}
		
		return o;
	}
	
	public boolean hasItems()
	{
		if( items.availablePermits() > 0 )
		{
			return true;
		}
		return false;
	}
	
	public boolean hasSlots()
	{
		if( slots.availablePermits() > 0 )
		{
			return true;
		}
		return false;
	}

	public int getCapacity() {
		return capacity;
	}

	public int checkSize() throws InterruptedException 
	{
		int i;
		listAccessor.acquire();
		i = itemList.size();
		listAccessor.release();
		return i;
	}
	

}
