package com.example.androidchattesting.Fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.androidchattesting.R;
import com.example.androidchattesting.Net.Packets.ChatPacket;


public class ChatMessageArrayAdapter extends ArrayAdapter<ChatPacket> {

	private Context context;
	private ArrayList<ChatPacket> chatList;
	
	public ChatMessageArrayAdapter(Context context, ArrayList<ChatPacket> chatList) {
		super(context,R.layout.default_listview_chatview, chatList);
		
		this.context = context;
		this.chatList = chatList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	    View rowView = inflater.inflate(R.layout.listview_chatmessage, parent, false);

	    
	    TextView nameTag = (TextView) rowView.findViewById(R.id.chatFragment_textView_nameTag);
	    TextView body = (TextView) rowView.findViewById(R.id.chatFragment_textView_textArea);
	    TextView infoStamp = (TextView) rowView.findViewById(R.id.chatFragment_textView_infoStamp);
	    	    
	    nameTag.setText(chatList.get(position).getUserName());
	    body.setText(chatList.get(position).getMessage());
	    

	    int height_in_pixels = 50 * body.getLineHeight(); //approx height text
	    body.setHeight(height_in_pixels);
	    
	    Date date = new Date();
	    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS");
	    String dateFormatted = formatter.format(date);
	    infoStamp.setText( dateFormatted );
	    
	    return rowView;	
	}

}
