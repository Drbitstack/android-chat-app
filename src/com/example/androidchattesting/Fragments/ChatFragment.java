package com.example.androidchattesting.Fragments;


import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;

import com.example.androidchattesting.MainActivity;
import com.example.androidchattesting.Net.ChatClient;
import com.example.androidchattesting.Net.ChatClientListener;
import com.example.androidchattesting.Net.Packets.ChatPacket;
import com.example.androidchattesting.Net.Packets.Packet;


public class ChatFragment extends ListFragment implements ChatClientListener {

	private ChatMessageArrayAdapter adapter;
	private ArrayList<ChatPacket> chatList = new ArrayList<ChatPacket>();
	private ChatClient client;
	
	MainActivity mCallback;
	
	public ChatFragment(){}
	
	//This fragment is notified about both service discovery and host discovery. 
	//A device populates this list only when it has been detected running the appropriate service.
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		client.addListener(this);
        adapter = new ChatMessageArrayAdapter( getActivity(), chatList );
        setListAdapter(adapter);
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (MainActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


	@Override
	public void OnMessageReceive(Packet receivedPacket) {
		
		if( receivedPacket instanceof ChatPacket )
		{
			chatList.add((ChatPacket)receivedPacket);
			mCallback.getHandler().post( new Runnable()
			{
						public void run()
							{
								
									adapter.notifyDataSetChanged();
							}
			});
		}

	}

	@Override
	public void OnMessageSend(Packet sentPacket) {
		if( sentPacket instanceof ChatPacket )
		{
			chatList.add((ChatPacket)sentPacket);
			mCallback.getHandler().post( new Runnable()
			{
						public void run()
							{
								
								adapter.notifyDataSetChanged();
							}
			});
		}
	}

	@Override
	public void OnStatusChange(int newStatus) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
